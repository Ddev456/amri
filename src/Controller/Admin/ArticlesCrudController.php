<?php

namespace App\Controller\Admin;

use App\Entity\Articles;
use App\Entity\Users;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use Vich\UploaderBundle\Form\Type\VichImageType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class ArticlesCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Articles::class;
    }

    public function configureCrud(Crud $crud): Crud
{
    return $crud
        ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig');
}

    public function configureActions(Actions $actions): Actions
    {
        return $actions
        ->add(Crud::PAGE_INDEX, Action::DETAIL)
        ->update(Crud::PAGE_INDEX, Action::NEW, function(Action $action){
            return $action->setIcon('fas fa-feather')->setLabel('Créer un article')->addCssClass('btn btn-success');
        })
        ->update(Crud::PAGE_INDEX, Action::EDIT, function(Action $action){
            return $action->setIcon('far fa-edit')->setLabel('Éditer')->addCssClass('btn btn-warning');
        })
        ->update(Crud::PAGE_INDEX, Action::DETAIL, function(Action $action){
            return $action->setIcon('fa fa-eye')->setLabel('Voir')->addCssClass('btn btn-info');
        })
        ->update(Crud::PAGE_INDEX, Action::DELETE, function(Action $action){
            return $action->setIcon('fa fa-trash')->setLabel('Supprimer')->addCssClass('btn btn-danger');
        });
    }

    // public function configureActions(Actions $actions): Actions
    // {
    //     // this action executes the 'renderInvoice()' method of the current CRUD controller
    //     $viewInvoice = Action::new('add', 'Ajouter un article', 'fas fa-feather')
    //         ->linkToCrudAction('renderInvoice');
    //     return $actions
    //         // ...
    //         ->add(Crud::PAGE_DETAIL, $viewInvoice)
    //     ;
    // }

    public function configureFields(string $pageName): iterable
    {
        $choices = $this->getDoctrine()->getRepository(Users::class)->findAll();
        // dd($choices[0]);
        return [
            TextField::new('title', 'Titre'),
            // AssociationField::new('title', 'Auteur')->setFormTypeOption(),
            SlugField::new('slug', 'URL')->setTargetFieldName('title')->hideOnIndex(),
            TextEditorField::new('content', 'Contenu')->setFormType(CKEditorType::class),
            NumberField::new('reading_time', 'Temps de lecture'),
            DateField::new('created_at', 'Mis en ligne le'),
            DateField::new('updated_at', 'Édité le')->onlyWhenUpdating(),
            BooleanField::new('display', 'En ligne'),
            TextField::new('imageFile')->setFormType(VichImageType::class)->hideOnIndex(),
            ImageField::new('featured_image', 'Image')->setBasePath('/uploads/images/featured/')->onlyOnIndex(),
         ];
    }
    
}
