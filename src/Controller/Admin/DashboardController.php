<?php

namespace App\Controller\Admin;

use App\Entity\Articles;
use App\Entity\Users;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use Symfony\Component\Security\Core\User\UserInterface;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Amri');
    }

    public function configureAssets(): Assets
    {
        return Assets::new()
        ->addCssFile(('bundles/easyadmin/css/style.css'));
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Articles & Revues', 'far fa-newspaper', Articles::class);
        yield MenuItem::linkToCrud('Utilisateurs', 'fas fa-users-cog', Users::class);
    }
    public function configureFields(string $pageName): iterable
{
    return [

        // native date picker format cannot be changed
        // Uses native HTML5 widgets when rendering this field in forms.
        DateTimeField::new('beginsAt')->setFormat('Y-MM-dd HH:mm')->renderAsNativeWidget(),

        // Uses <select> lists when rendering this field in forms.
        DateTimeField::new('beginsAt')->setFormat('Y-MM-dd HH:mm')->renderAsChoice(),

        // Uses <input type="text"> elements when rendering this field in forms.
        DateTimeField::new('beginsAt')->setFormat('Y-MM-dd HH:mm')->renderAsText(),
    ];
}

    public function configureUserMenu(UserInterface $user): UserMenu
        {
            // Usually it's better to call the parent method because that gives you a
            // user menu with some menu items already created ("sign out", "exit impersonation", etc.)
            // if you prefer to create the user menu from scratch, use: return UserMenu::new()->...
            return parent::configureUserMenu($user)
                // use the given $user object to get the user name
                ->setName($user-> getUserIdentifier())
                // use this method if you don't want to display the name of the user
                // ->displayUserName(false)

                // you can return an URL with the avatar image
                // ->setAvatarUrl('https://...')
                // ->setAvatarUrl($user->getProfileImageUrl())
                // use this method if you don't want to display the user image
                // ->displayUserAvatar(false)
                // you can also pass an email address to use gravatar's service
                // ->setGravatarEmail($user->getMainEmailAddress())

                // you can use any type of menu item, except submenus
                ->addMenuItems([
                    MenuItem::linkToRoute('My Profile', 'fa fa-id-card', '...', ['...' => '...']),
                    MenuItem::linkToRoute('Settings', 'fa fa-user-cog', 'settings'),
                    MenuItem::section(),
                    MenuItem::linkToLogout('Logout', 'fa fa-sign-out'),
                ]);
        }
}
