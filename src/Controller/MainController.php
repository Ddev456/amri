<?php

namespace App\Controller;

use App\Repository\ArticlesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="main")
     */
    public function index(ArticlesRepository $articles): Response
    {
        return $this->render('main/index.html.twig', [
            'articles' => $articles->findBy(['display' => true], ['created_at' => 'desc']),
            'featured' => $articles->findBy(['display' => true], ['created_at' => 'desc'], 1, 0)
        ]);
    }
}
