<?php

namespace App\Controller;

use App\Entity\Articles;
use App\Repository\ArticlesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    /**
     * @Route("/article/{slug}", name="article")
     */
    public function index($slug, ArticlesRepository $articles): Response
    {
        $articles = $this->getDoctrine()->getRepository(Articles::class)->findby(['slug' => $slug ]);
        return $this->render('article/index.html.twig', [
            'articles' => $articles,
        ]);
    }
}
